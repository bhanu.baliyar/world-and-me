# Intro

World and Me is an Android app that fetches the country capitals and additional details to display in a RecyclerView


# Description
The App is designed keeping the clean architecture picture in mind, uses MVI to track states to the view changes and the also actions,
accordingly. Since we are not using any form of DI we make use of an AppContainer scoped to the application which manages the dependencies
for us. There is a lot that can be done especially with the scoping, since this is a small all the flow is scoped to the entire Application.


# Features
* This loads results from the api `https://gist.githubusercontent.com/peymano-wmt/32dcb892b06648910ddd40406e37fdab/raw/db25946fd77c5873b0303b858e861ce724e0dcd0/countries.json`
  into a recycle view
* Pull to refresh supported for refreshing data, ideally we want to fetch from local cache instead of making the call over wire (TODO)

The app is divided into 4 parts
  * data
  * domain
  * presentation
  * core


## Data
Has the DTOs and any extensions that we use to convert from one type to another, it also will host the remote and local data scources.

## Domain
Has the business logic for the app and is the main brain of the app

## Presentation
Has the view components, activities, fragments, recycler views, view models, the flow of data is from the presentation to domain to data.

## Core 
Core hosts common functionality which will be used across the app, a good candidate for a library which can be reused by other apps or libraries.

A view model test has also been included using the Mockk library


# TODOs
  * One of the key things that can done is cache the network results using Room db so we do not have to fetch this over and over again when the app 
    restarts.
  * Also using a DI makes things easier
  * Instrumentation tests
  * UI could be worked on with the help of a Product manager
  * This solution loads 200+ items in a recycle view, ideally we would want some sort if pagination technique, client side or server side.

