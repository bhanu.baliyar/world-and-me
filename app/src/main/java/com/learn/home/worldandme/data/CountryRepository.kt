package com.learn.home.worldandme.data

import com.learn.home.worldandme.core.BaseRepository
import com.learn.home.worldandme.core.ResultWrapper
import com.learn.home.worldandme.core.network.CountriesApi
import com.learn.home.worldandme.data.country.CountryItem

class CountryRepository(
    private val api: CountriesApi
) : BaseRepository() {


    suspend fun getCountries(): ResultWrapper<List<CountryItem>> =
        invoke {
            api.getCountries()
        }


}