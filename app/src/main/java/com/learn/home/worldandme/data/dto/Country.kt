package com.learn.home.worldandme.data.dto

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
typealias Countries = List<Country>
@Parcelize
data class Country(val name: String?, val region: String?, val code: String?, val capital: String?) :
    Parcelable