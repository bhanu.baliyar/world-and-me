package com.learn.home.worldandme.presentation.country

import androidx.recyclerview.widget.RecyclerView
import com.learn.home.worldandme.data.dto.Country
import com.learn.home.worldandme.databinding.CountriesListItemBinding

/**
 * View holder for the list of countries to be shown, since we are using databinding,
 * we can just assign the dto to the country variable.
 */
class CountriesViewHolder(private val binding: CountriesListItemBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(countryDto: Country) {
        with(binding) {
            country = countryDto
        }
    }
}