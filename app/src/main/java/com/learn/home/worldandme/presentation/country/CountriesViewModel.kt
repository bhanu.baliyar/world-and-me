package com.learn.home.worldandme.presentation.country

import androidx.lifecycle.ViewModel

import androidx.lifecycle.viewModelScope

import com.learn.home.worldandme.core.ResultWrapper
import com.learn.home.worldandme.domain.country.GetCountriesUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch


/**
 * The jetpack viewmodel is designed to survive orientation change.
 */
class CountriesViewModel(private val countriesUseCase: GetCountriesUseCase) :
    ViewModel() {
    private val _state: MutableStateFlow<CountriesFragmentState> =
        MutableStateFlow(CountriesFragmentState.Idle)
    val state: StateFlow<CountriesFragmentState>
        get() = _state


    /**
     * Launch on load.
     */
    init {
        handlerIntent()
    }

    private fun handlerIntent() {
        loadCountries()
    }

    fun loadCountries() {
        showProgressBar()
        viewModelScope.launch {
            _state.value = when (val result = countriesUseCase.getCountries()) {
                is ResultWrapper.Success -> CountriesFragmentState.Success(result.value)
                is ResultWrapper.NetworkError -> CountriesFragmentState.Error
                is ResultWrapper.GenericError -> CountriesFragmentState.Error
                else -> {
                    CountriesFragmentState.Error
                }
            }
        }
    }

    /**
     * Show progress bar.
     */
    private fun showProgressBar() {
        viewModelScope.launch { _state.emit(CountriesFragmentState.InFlight) }
    }
}