package com.learn.home.worldandme.domain.country

import com.learn.home.worldandme.core.ResultWrapper
import com.learn.home.worldandme.data.dto.Countries

/**
 * Countries use case, returns a ResultWrapper.
 */
interface GetCountriesUseCase {

    /**
     * Get countries use case.
     */
    suspend fun getCountries() : ResultWrapper<Countries>

}