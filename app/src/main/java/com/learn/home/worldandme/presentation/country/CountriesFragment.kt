package com.learn.home.worldandme.presentation.country

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.learn.home.worldandme.R
import com.learn.home.worldandme.core.WorldAndMeViewModelFactory
import com.learn.home.worldandme.data.dto.Countries
import com.learn.home.worldandme.databinding.FragmentCountriesBinding

import kotlinx.coroutines.launch

/**
 * Sealed class to track the state of our view.
 */
sealed class CountriesFragmentState {
    data class Success(val networkResults: Countries) : CountriesFragmentState()

    object InFlight : CountriesFragmentState()
    object Error : CountriesFragmentState()
    object Idle : CountriesFragmentState()
    object Empty : CountriesFragmentState()
}


class CountriesFragment : Fragment() {
    /**
     * FragmentCountriesBinding
     */
    private lateinit var binding: FragmentCountriesBinding

    /**
     * CountriesAdapter
     */
    private lateinit var countriesAdapter: CountriesAdapter

    /**
     * CountriesViewModel
     */
    private val countriesViewModel: CountriesViewModel by viewModels { WorldAndMeViewModelFactory }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_countries, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialize()
        subscribe()
    }

    private fun initialize() {
        countriesAdapter = CountriesAdapter()

        with(binding) {
            countriesList.adapter = countriesAdapter
            swipeRefresh.setOnRefreshListener {
                lifecycleScope.launch {
                    countriesViewModel.loadCountries()
                }
            }
        }
    }

    private fun subscribe() {
        lifecycleScope.launch {
            countriesViewModel.state.collect { state ->
                when (state) {
                    is CountriesFragmentState.Success -> loadResults(state.networkResults)
                    is CountriesFragmentState.InFlight -> toggleProgressBar(true)
                    is CountriesFragmentState.Error -> loadError()
                    is CountriesFragmentState.Idle -> toggleProgressBar(false)
                    else -> {
                        loadError()
                    }
                }
            }
        }
    }


    /**
     * Load an error message.
     */
    private fun loadError() {
        toggleProgressBar(false)
        binding.countriesList.visibility = View.GONE
        binding.errorLayout.visibility = View.VISIBLE
        context?.let {
            Glide.with(it)
                .load(R.drawable.error)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(binding.errorImage)
        }
    }


    /**
     * Load results.
     */
    private fun loadResults(networkResults: Countries) {
        binding.errorLayout.visibility = View.GONE
        binding.countriesList.visibility = View.VISIBLE
        countriesAdapter.submitList(networkResults)
        toggleProgressBar(false) // hide progress bar
    }

    /**
     * Toggle progress bar
     */
    private fun toggleProgressBar(show: Boolean) {
        binding.swipeRefresh.isRefreshing = show
    }
}