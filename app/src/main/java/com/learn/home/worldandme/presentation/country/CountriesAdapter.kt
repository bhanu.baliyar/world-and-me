package com.learn.home.worldandme.presentation.country

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.learn.home.worldandme.R
import com.learn.home.worldandme.data.dto.Country
import com.learn.home.worldandme.databinding.CountriesListItemBinding

class CountriesAdapter : ListAdapter<Country, CountriesViewHolder>(COMPARATOR) {
    companion object {
        private val COMPARATOR = object : DiffUtil.ItemCallback<Country>() {
            override fun areItemsTheSame(
                oldItem: Country,
                newItem: Country
            ): Boolean = oldItem.name == newItem.name


            override fun areContentsTheSame(
                oldItem: Country,
                newItem: Country
            ): Boolean =
                oldItem.name == newItem.name
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountriesViewHolder {
        val binding: CountriesListItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.countries_list_item,
            parent, false
        )
        return CountriesViewHolder(
            binding
        )
    }

    override fun onBindViewHolder(holder: CountriesViewHolder, position: Int) {
        val country = getItem(position)
        if (country != null) {
            holder.bind(country)
        }
    }
}