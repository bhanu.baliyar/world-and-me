package com.learn.home.worldandme.core

import com.learn.home.worldandme.core.network.CountriesApi
import com.learn.home.worldandme.core.network.Endpoints
import com.learn.home.worldandme.data.CountryRepository
import com.learn.home.worldandme.domain.country.GetCountriesUseCaseImpl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * The AppContainer is scoped to the application, although as the application grows in size, each feature
 * can be divided in flows which in turn can be scoped to a view/fragment/activity.
 */
class AppContainer {

    private val logging = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }
    private val okHttp = OkHttpClient.Builder()
        .addInterceptor(logging)
        .build()


    private val retrofit: Retrofit = Retrofit.Builder()
        .client(okHttp)
        .baseUrl(Endpoints.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()


    private val countriesApi: CountriesApi = retrofit.create(CountriesApi::class.java)

    private val countryRepository = CountryRepository(countriesApi)

    val countryUseCase = GetCountriesUseCaseImpl(countryRepository)

}