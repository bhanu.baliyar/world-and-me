package com.learn.home.worldandme.core

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProvider.AndroidViewModelFactory.Companion.APPLICATION_KEY
import androidx.lifecycle.viewmodel.CreationExtras
import com.learn.home.worldandme.presentation.country.CountriesViewModel

/**
 * Factory for any viewmodel that we might create in future.
 */
@Suppress("UNCHECKED_CAST")
val WorldAndMeViewModelFactory = object : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>, extras: CreationExtras): T =
        with(modelClass) {
            val application = checkNotNull(extras[APPLICATION_KEY]) as CountriesApplication
            val useCase = application.appContainer.countryUseCase
            when {
                isAssignableFrom(CountriesViewModel::class.java) ->
                    CountriesViewModel(countriesUseCase = useCase)

                else ->
                    throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
            }
        } as T
}
