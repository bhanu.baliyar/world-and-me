package com.learn.home.worldandme.data.country


import com.google.gson.annotations.SerializedName
import com.learn.home.worldandme.data.dto.Countries
import com.learn.home.worldandme.data.dto.Country

data class CountryItem(
    @SerializedName("capital")
    val capital: String?,
    @SerializedName("code")
    val code: String?,
    @SerializedName("currency")
    val currency: Currency?,
    @SerializedName("demonym")
    val demonym: String?,
    @SerializedName("flag")
    val flag: String?,
    @SerializedName("language")
    val language: Language?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("region")
    val region: String?
)

fun List<CountryItem>.toCountries(): Countries {
    return this.map { country ->
        Country(
            name = country.name,
            region = country.region,
            code = country.code,
            capital = country.capital
        )
    }

}