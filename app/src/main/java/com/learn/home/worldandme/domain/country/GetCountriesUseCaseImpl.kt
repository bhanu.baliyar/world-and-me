package com.learn.home.worldandme.domain.country

import com.learn.home.worldandme.core.ResultWrapper
import com.learn.home.worldandme.data.CountryRepository
import com.learn.home.worldandme.data.country.toCountries
import com.learn.home.worldandme.data.dto.Countries

class GetCountriesUseCaseImpl(
    private val countryRepository: CountryRepository
) : GetCountriesUseCase {
    override suspend fun getCountries(): ResultWrapper<Countries> {
        return when (val result = countryRepository.getCountries()) {
            is ResultWrapper.Success -> ResultWrapper.Success(result.value.toCountries())
            is ResultWrapper.GenericError -> ResultWrapper.GenericError()
            else -> ResultWrapper.EmptyResponse
        }
    }
}