package com.learn.home.worldandme.core.network

import com.learn.home.worldandme.data.country.CountryItem
import retrofit2.http.GET

interface CountriesApi {
    @GET(Endpoints.COUNTRIES)
    suspend fun getCountries(): List<CountryItem>
}