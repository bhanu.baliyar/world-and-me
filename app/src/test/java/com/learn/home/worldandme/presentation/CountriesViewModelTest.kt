package com.learn.home.worldandme.presentation

import com.learn.home.worldandme.MainDispatcherRule
import com.learn.home.worldandme.core.ResultWrapper
import com.learn.home.worldandme.data.dto.Country
import com.learn.home.worldandme.domain.country.GetCountriesUseCase
import com.learn.home.worldandme.presentation.country.CountriesFragmentState
import com.learn.home.worldandme.presentation.country.CountriesViewModel
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import io.mockk.junit4.MockKRule
import io.mockk.unmockkObject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class CountriesViewModelTest {

    @get:Rule
    val mockkRule = MockKRule(this)

    @OptIn(ExperimentalCoroutinesApi::class)
    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @MockK
    lateinit var getCountriesUseCaseMock: GetCountriesUseCase

    private lateinit var countriesViewModel: CountriesViewModel


    private val fakeResults = listOf(
        Country(
            name = "fakeCountry",
            capital = "fakeCapital",
            region = "fakeRegion",
            code = "fakeCode"
        )
    )

    @Before
    fun setUp() {
        coEvery { getCountriesUseCaseMock.getCountries() } returns ResultWrapper.Success(fakeResults)
        countriesViewModel = CountriesViewModel(getCountriesUseCaseMock)

    }


    @OptIn(ExperimentalCoroutinesApi::class)
    @After
    fun tearDown() {
        unmockkObject(getCountriesUseCaseMock)
        Dispatchers.resetMain()
    }


    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun `test get countries returns results`() = runTest {
        coVerify(exactly = 1) { getCountriesUseCaseMock.getCountries() }
        Assert.assertNotNull(countriesViewModel.state)
        Assert.assertEquals(
            countriesViewModel.state.value,
            CountriesFragmentState.Success(fakeResults)
        )
    }

}